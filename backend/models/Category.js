const mongoose = require('mongoose')

const categorySchema = new mongoose.Schema({
    type:{
        type: String
    },
    description:{
        type:String
    }
});

module.exports = mongoose.model('category', categorySchema)